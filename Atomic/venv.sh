#!/bin/bash

VENV=$1

source ~/.virtualenvs/$VENV/bin/activate

# add alias venv='source /usr/local/bin/venv $1' to bashrc
