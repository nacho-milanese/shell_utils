#!/usr/bin/env python
import os
from sys import argv
from os.path import expanduser


class virtualenv(object):
    """Defines a virtualenv on a list of dirs and excecutes the chosen passed
    as a parameter in number"""

    def __init__(self):
        # by default, goes to home / sources
        self.home = expanduser("~")
        self.sources = self.home + "/.virtualenvs/"
        self.original = os.getcwd()

    def creat_venv_default(self):
        """ Creates the venv dir and the default virtualenv"""
        os.mkdir(self.sources)
        os.chdir(self.sources)
        os.system("virtualenv default")

    def get_venv(self):
        """get venv list in given dir"""
        if not os.path.isdir(self.sources):
            self.creat_venv_default()
        venvs = os.listdir(self.sources)
        os.chdir(self.original)
        return venvs

    def list_venv(self):
        """list all venvs available and it's number option"""
        venvs = self.get_venv()
        for members in venvs:
            print(venvs.index(members), " - ", members)

    def set_venv(self, virtenv=-1):
        """sets the venv and runs atom"""
        venvs = self.get_venv()
        venvs.append("default")
        virtenv = int(virtenv)
        print("{} virtualenv chosen".format(venvs[virtenv]))
        os.chdir(self.original)
        os.system("source {0}{1}/bin/activate && atom {2}".format(
            self.sources,
            venvs[virtenv],
            self.original))


if __name__ == '__main__':
    venv = virtualenv()
    if len(argv) < 2:
        venv.list_venv()
    else:
        venv.set_venv(argv[1])
