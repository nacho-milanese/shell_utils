# shell_utils
  
Personal shell helpers and utils

## atomic:

Launches atom in the current working directory using a given python virtualenv.

Virtualenv location is defined in `self.sources` variable in the init method.

With no options, it lists all the current virutalenvs
accepts an int for the virtualenv chosen
-1 triggers the default (should exist or will create it)

## magic_prompt:

Creates a prompt that changes according to exit status
*** TESTED ON OS X ONLY ***

### INSTALLATION:

 1. copy magic_prompt file to /usr/local/bin
 2. make file executable
 3. add the following lines at the top of the ~/.bashrc file: 
 
    `### Default prompt called:`

    `PS1='\[\e[1;34m\]\w\[\e[m\] 👉  \[\e[1;32m\]\$\[\e[m\] \[\e[0;37m\]'`

    `# ### import magic prompt:`

    `source /usr/local/bin/magic_prompt`


